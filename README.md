# ISTI docs

This is a repository to store useful documentation for ISTI tools.

Please read the [WIKI](https://gitlab.com/w895/centro_calcolo_isti/-/wikis/home)

## Contacts

For any problem or suggestion, feel free to [open an issue](https://gitlab.com/w895/centro_calcolo_isti/-/issues/new) assigned to Andrea Berti, with a brief description of the problem (and a possible solution if you already have one).

If you want to actvely contribute to the developement of this WIKI repository, [contact me](mailto:andrea.berti@isti.cnr.it) with an email.
